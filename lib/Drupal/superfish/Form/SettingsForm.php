<?php
namespace \Drupal\superfish\Form;
use Drupal\Core\Form\ConfigFormBase;

class SettingsForm extends ConfigFormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormID() {
    return 'superfish_admin_settings';
  }

  public function buildForm(array $form, array &$form_state) {
    $form['superfish_number'] = array(
      '#type' => 'select',
      '#title' => t('Number of blocks'),
      '#multiple' => FALSE,
      '#options' => drupal_map_assoc(range(1, 50)),
      '#description' => t('The number of Superfish menu blocks.') . ' (' . t('Default') . ': 4' . ')' . '<br />' . t('Please note decreasing this number leads to permanent deletion of blocks.'),
      '#default_value' => variable_get('superfish_number', 4),
    );
    $form['superfish_slp'] = array(
      '#type' => 'textarea',
      '#title' => t('Path to Superfish library'),
      '#description' => t('Edit only if you are sure of what you are doing.'),
      '#default_value' => variable_get('superfish_slp', superfish_library_path()),
      '#rows' => 7,
    );
    return parent::buildForm($form, $form_state);
  }

  public function validateForm(array &$form, array &$form_state) {
    parent::validateForm($form, $form_state);

    $error = array();
    $sf_library = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", trim($form_state['values']['superfish_slp']));
    if (empty($sf_library)) {
      form_set_error('superfish_slp', t('<strong>Path to Superfish library</strong> field cannot be empty. Please try the below list:') . '<br /><pre>' . superfish_library_path() . '</pre>');
    }
    else {
      // Trimming blank lines and such
      $sf_library = explode("\n", $sf_library);
      // Crystal clear
      foreach ($sf_library as $s) {
        if (!file_exists($s)) {
          $error[] = $s;
        }
      }
      if (!empty($error)) {
        $error_message = '';
        if (count($error) > 1) {
          foreach ($error as $e) {
            $error_message .= '<li>' . $e . '</li>';
          }
          $error_message = t('Files not found') . ': <ul>' . $error_message . '</ul>';
        }
        else {
          $error_message = t('File not found') . ': ' . $error[0];
        }
        form_set_error('superfish_slp', $error_message);
      }
    }

  }

  public function submitForm(array &$form, array &$form_state) {
    $values = &$form_state['values'];
    $before = variable_get('superfish_number', 4);
    $after = $values['superfish_number'];
    // If the number of blocks has been decreased.
    if ($before > $after) {
      // How many blocks should be removed?
      $reduce = $before - $after;
      // Remove each block with all its variables.
      $delta = $after;
      for ($i = 0; $i < $reduce; $i++) {
        $delta++;
        db_delete('variable')->condition('name', 'superfish_%%_' . $delta , 'LIKE')->execute();
        db_delete('block')->condition('module', 'superfish')->condition('delta', $delta)->execute();
        db_delete('block_role')->condition('module', 'superfish')->condition('delta', $delta)->execute();
      }
      drupal_set_message(t('Successfully removed @number Superfish block(s).', array('@number' => $reduce)));
    }

    parent::submitForm($form, $form_state);
  }
}