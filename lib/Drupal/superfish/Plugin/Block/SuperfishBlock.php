<?php

namespace Drupal\superfish\Plugin\Block;


use Drupal\block\BlockBase;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides block with superfish menu.
 *
 * @Block(
 *   id = "superfish_block",
 *   admin_label = @Translation("Superfish menu")
 * )
 */
class SuperfishBlock extends BlockBase implements ContainerFactoryPluginInterface {

  public function __construct(array $configuration, $plugin_id, array $plugin_definition, ConfigFactory $config) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, array $plugin_definition) {
    return new static (
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * Builds and returns the renderable array for this block plugin.
   *
   * @return array
   *   A renderable array representing the content of the block.
   *
   * @see \Drupal\block\BlockRenderController
   */
  public function build() {
    $build['content'] = array();
    return $build;
  }


  /**
   * {@inheritdoc}
   */
  public function blockForm($form, &$form_state) {
    $form = array();

    $config = $this->configuration;
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Block description'),
      '#description' => t('A brief description of your block. Used on the !link', array('!link' => l(t('Blocks administration page'), 'admin/structure/block'))),
      '#default_value' => $config['name'],
    );
    $form['sf-menu'] = array(
      '#type' => 'details',
      '#title' => t('Menu'),
      '#open' => TRUE,
    );
    $form['sf-menu']['menu'] = array(
      '#type' => 'select',
      '#title' => t('Menu parent'),
      '#description' => t('The menu you want to be displayed using Superfish.') . ' <em>(' . t('Default') . ': &lt;Main menu&gt;)</em>',
      '#default_value' => $config['menu'],
      '#options' => menu_parent_options(menu_get_menus(), array('mlid' => 0)),
    );
    $form['sf-menu']['depth'] = array(
      '#type' => 'select',
      '#title' => t('Menu depth'),
      '#description' => t('The number of child levels starting with the parent selected above. <strong>-1</strong> means all of them, <strong>0</strong> means none of them.') . ' <em>(' . t('Default') . ': -1)</em>',
      '#default_value' => $config['depth'],
      '#options' => drupal_map_assoc(range(-1, 50)),
    );
    $form['sf-menu']['expanded'] = array(
      '#type' => 'checkbox',
      '#title' => t('Take "Expanded" option into effect.'),
      '#description' => t('By enabling this option, only parent menu items with <em>Expanded</em> option enabled will have their submenus appear.') . ' <em>(' . t('Default') . ': ' . t('disabled') . ')</em>',
      '#default_value' => $config['expanded'],
    );
    $form['sf-settings'] = array(
      '#type' => 'details',
      '#title' => t('Superfish settings'),
      '#open' => TRUE,
    );
    $form['sf-settings']['type'] = array(
      '#type' => 'select',
      '#title' => t('Menu type'),
      '#description' => '<em>(' . t('Default') . ': ' . t('Horizontal') . ')</em>',
      '#default_value' => $config['type'],
      '#options' => array(
        'horizontal' => t('Horizontal'),
        'vertical' => t('Vertical'),
        'navbar' => t('NavBar'),
      ),
    );
    $form['sf-settings']['style'] = array(
      '#type' => 'select',
      '#title' => t('Style'),
      '#description' => '<em>(' . t('Default') . ': ' . t('None') . ')</em>',
      '#default_value' => $config['style'],
      '#options' => superfish_styles(),
    );
    $form['sf-settings']['speed'] = array(
      '#type' => 'textfield',
      '#title' => t('Animation speed'),
      '#description' => t('The speed of the animation either in <strong>milliseconds</strong> or pre-defined values (<strong>slow, normal, fast</strong>).') . ' <em>(' . t('Default') . ': fast)</em>',
      '#default_value' => $config['speed'],
      '#size' => 15,
    );
    $form['sf-settings']['delay'] = array(
      '#type' => 'textfield',
      '#title' => t('Mouse delay'),
      '#description' => t('The delay in <strong>milliseconds</strong> that the mouse can remain outside a sub-menu without it closing.') . ' <em>(' . t('Default') . ': 800)</em>',
      '#default_value' => $config['delay'],
      '#size' => 15,
    );
    $form['sf-settings']['pathclass'] = array(
      '#type' => 'textfield',
      '#title' => t('Path class'),
      '#description' => t('The class you have applied to list items that lead to the current page.') . ' <em>(' . t('Default') . ': active-trail)</em><br />' . t('Change this <strong>only</strong> if you are <strong>totally sure</strong> of what you are doing.'),
      '#default_value' => $config['pathclass'],
      '#size' => 15,
    );
    $form['sf-settings']['pathlevels'] = array(
      '#type' => 'select',
      '#title' => t('Path levels'),
      '#description' => t('The amount of sub-menu levels that remain open or are restored using <strong>Path class</strong>.') . ' <em>(' . t('Default') . ': 1)</em>',
      '#default_value' => $config['pathlevels'],
      '#options' => drupal_map_assoc(range(0, 10)),
    );
    $form['sf-settings']['slide'] = array(
      '#type' => 'select',
      '#title' => t('Slide-in effect'),
      '#description' => '<em>(' . t('Default') . ': ' . t('Vertical') . ')</em><br />' . ((count(superfish_effects()) == 4) ? t('jQuery Easing plugin is not installed.') . '<br />' . t('The plugin provides a handful number of animation effects, they can be used by uploading the \'jquery.easing.js\' file to the libraries directory within the \'easing\' directory (for example: sites/all/libraries/easing/jquery.easing.js). Refresh this page after the plugin is uploaded, this will make more effects available in the above list.') . '<br />' : '') . '<strong>' . t('Important') . ':</strong> ' . t('Easing effects require jQuery 1.6.1 or higher. If you need to update your jQuery to version 1.6.1 or higher, please use the jQuery Update module.'),
      '#default_value' => $config['slide'],
      '#options' => superfish_effects(),
    );
    $form['sf-settings']['arrow'] = array(
      '#type' => 'checkbox',
      '#title' => t('Auto-arrows'),
      '#default_value' => $config['arrow'],
    );
    $form['sf-settings']['shadow'] = array(
      '#type' => 'checkbox',
      '#title' => t('Drop shadows'),
      '#default_value' => $config['shadow'],
    );
    $form['sf-settings']['sf-more'] = array(
      '#type' => 'details',
      '#title' => t('More options'),
    );
    $form['sf-settings']['sf-more']['use_link_theme'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use a theme function for hyperlinks.') . ' <em>(' . t('Default') . ': ' . t('enabled') . ')</em><br /><small>' . t('(Disabling this feature can result in performance improvements, depending on the number of hyperlinks in the menu.)') . '</small>',
      '#default_value' => $config['use_link_theme'],
    );
    $form['sf-settings']['sf-more']['use_item_theme'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use a theme function for menu items.') . ' <em>(' . t('Default') . ': ' . t('enabled') . ')</em><br /><small>' . t('(Disabling this feature can result in performance improvements, depending on the number of items in the menu.)') . '</small>',
      '#default_value' => $config['use_item_theme'],
    );
    $form['sf-settings']['sf-more']['clone_parent'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add cloned parent links to the top of sub-menus.') . ' <em>(' . t('Default') . ': ' . t('disabled') . ')</em>',
      '#default_value' => $config['clone_parent'],
    );
    $form['sf-plugins'] = array(
      '#type' => 'details',
      '#title' => t('Superfish plugins'),
    );
    $form['sf-plugins']['bgf'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use jQuery BgiFrame plugin for this menu.'),
      '#description' => t('Helps ease the pain when having to deal with IE z-index issues.') . ' <em>(' . t('Default') . ': ' . t('disabled') . ')</em>',
      '#default_value' => $config['bgf'],
    );
    $form['sf-plugins']['spp'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use jQuery Supposition plugin for this menu.') . ' <sup>(' . t('Beta') . ')</sup>',
      '#description' => t('Relocates sub-menus when they would otherwise appear outside the browser window area.') . ' <em>(' . t('Default') . ': ' . t('enabled') . ')</em>',
      '#default_value' => $config['spp'],
    );
    $form['sf-plugins']['hid'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable hoverIntent detection.'),
      '#description' => t('Prevents accidental firing of animations by waiting until the user\'s mouse slows down enough, hence determinig user\'s <em>intent</em>.') . ' <em>(' . t('Default') . ': ' . t('enabled') . ')</em>',
      '#default_value' => $config['hid'],
    );
    $form['sf-plugins']['amw'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use sf-AutomaticWidth plugin for this menu.'),
      '#description' => t('Automatically adjusts the menu width to maximum possible.') . ' <em>(' . t('Default') . ': ' . t('disabled') . ')</em><br />' . t('This is limited to Horizontal and NavBar types.'),
      '#default_value' => $config['amw'],
    );
    $form['sf-plugins']['sf-touchscreen'] = array(
      '#type' => 'details',
      '#title' => t('sf-Touchscreen') . ' <sup>(' . t('Beta') . ')</sup>',
      '#description' => t('<strong>sf-Touchscreen</strong> provides touchscreen compatibility for your menus.') . ' <sup>(' . t('The first click on a parent hyperlink shows its children and the second click opens the hyperlink.') . ')</sup>',
      '#open' => TRUE,
    );
    $form['sf-plugins']['sf-touchscreen']['touch'] = array(
      '#type' => 'radios',
      '#default_value' => $config['touch'],
      '#options' => array(
        0 => t('Disable') . '. <sup>(' . t('Default') . ')</sup>',
        1 => t('Enable jQuery sf-Touchscreen plugin for this menu.'),
        2 => t('Enable jQuery sf-Touchscreen plugin for this menu depending on the user\'s Web browser <strong>window width</strong>.'),
        3 => t('Enable jQuery sf-Touchscreen plugin for this menu depending on the user\'s Web browser <strong>user agent</strong>.'),
      ),
    );
    $form['sf-plugins']['sf-touchscreen']['touchbh'] = array(
      '#type' => 'radios',
      '#title' => 'Select a behaviour',
      '#description' => t('Using this plugin, the first click or tap will expand the sub-menu, here you can choose what a second click or tap should do.'),
      '#default_value' => $config['touchbh'],
      '#options' => array(
        0 => t('Opening the parent menu item link on the second tap.'),
        1 => t('Hiding the sub-menu on the second tap.'),
        2 => t('Hiding the sub-menu on the second tap, adding cloned parent links to the top of sub-menus as well.') . ' <sup>(' . t('Default') . ')</sup>',
      ),
    );
    $form['sf-plugins']['sf-touchscreen']['sf-touchscreen-windowwidth'] = array(
      '#type' => 'details',
      '#title' => t('Window width settings'),
      '#description' => t('sf-Touchscreen will be enabled only if the width of user\'s Web browser window is smaller than the below value.') . '<br /><br />' . t('Please note that in most cases such a meta tag is necessary for this feature to work properly:') . '<br /><code>&lt;meta name="viewport" content="width=device-width, initial-scale=1.0" /&gt;</code>',
    );
    $form['sf-plugins']['sf-touchscreen']['sf-touchscreen-windowwidth']['touchbp'] = array(
      '#type' => 'textfield',
      '#description' => t('Also known as "Breakpoint".') . ' <em>(' . t('Default') . ': 768)</em>',
      '#default_value' => $config['touchbp'],
      '#field_suffix' => t('pixels'),
      '#size' => 10,
    );
    $form['sf-plugins']['sf-touchscreen']['sf-touchscreen-useragent'] = array(
      '#type' => 'details',
      '#title' => t('User agent settings'),
    );
    $form['sf-plugins']['sf-touchscreen']['sf-touchscreen-useragent']['touchua'] = array(
      '#type' => 'radios',
      '#default_value' => $config['touchua'],
      '#options' => array(
        0 => t('Use the pre-defined list of the <strong>user agents</strong>.') . '<sup>(' . t('Default') . ') (' . t('Recommended') . ')</sup>',
        1 => t('Use the custom list of the <strong>user agents</strong>.'),
      ),
    );
    $form['sf-plugins']['sf-touchscreen']['sf-touchscreen-useragent']['touchual'] = array(
      '#type' => 'textfield',
      '#title' => t('Custom list of the user agents'),
      '#description' => t('Could be partial or complete. (Asterisk separated)') . ' <em>(' . t('Default') . ': ' . t('empty') . ')</em><br />' . t('Examples') . ':<ul><li>iPhone*Android*iPad <sup>(' . t('Recommended') . ')</sup></li><li>Mozilla/5.0 (webOS/1.4.0; U; en-US) AppleWebKit/532.2 (KHTML, like Gecko) Version/1.0 Safari/532.2 Pre/1.0 * Mozilla/5.0 (iPad; U; CPU OS 3_2_1 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Mobile/7B405</li></ul>' . ((isset($_SERVER['HTTP_USER_AGENT'])) ? '<br />' . t('<strong>UA string of the current Web browser:</strong>') . ' ' . $_SERVER['HTTP_USER_AGENT'] : ''),
      '#default_value' => $config['touchual'],
      '#size' => 100,
      '#maxlength' => 2000,
    );
    $form['sf-plugins']['sf-touchscreen']['sf-touchscreen-useragent']['touchuam'] = array(
      '#type' => 'select',
      '#title' => t('<strong>User agent</strong> detection method'),
      '#description' => '<em>(' . t('Default') . ': ' . t('Client-side (JavaScript)') . ')</em>',
      '#default_value' => $config['touchuam'],
      '#options' => array(
        0 => t('Client-side (JavaScript)'),
        1 => t('Server-side (PHP)')
      ),
    );
    $form['sf-plugins']['sf-smallscreen'] = array(
      '#type' => 'details',
      '#title' => t('sf-Smallscreen') . ' <sup>(' . t('Beta') . ')</sup>',
      '#description' => t('<strong>sf-Smallscreen</strong> provides small-screen compatibility for your menus.') . ' <sup>(' . t('Converts the dropdown into a &lt;select&gt; element.') . ')</sup>',
      '#open' => TRUE,
    );
    $form['sf-plugins']['sf-smallscreen']['small'] = array(
      '#type' => 'radios',
      '#default_value' => $config['small'],
      '#options' => array(
        0 => t('Disable') . '.',
        1 => t('Enable jQuery sf-Smallscreen plugin for this menu.'),
        2 => t('Enable jQuery sf-Smallscreen plugin for this menu depending on the user\'s Web browser <strong>window width</strong>.') . ' <sup>(' . t('Default') . ')</sup>',
        3 => t('Enable jQuery sf-Smallscreen plugin for this menu depending on the user\'s Web browser <strong>user agent</strong>.'),
      ),
    );
    $form['sf-plugins']['sf-smallscreen']['sf-smallscreen-windowwidth'] = array(
      '#type' => 'details',
      '#title' => t('Window width settings'),
      '#description' => t('sf-Smallscreen will be enabled only if the width of user\'s Web browser window is smaller than the below value.') . '<br /><br />' . t('Please note that in most cases such a meta tag is necessary for this feature to work properly:') . '<br /><code>&lt;meta name="viewport" content="width=device-width, initial-scale=1.0" /&gt;</code>',
    );
    $form['sf-plugins']['sf-smallscreen']['sf-smallscreen-windowwidth']['smallbp'] = array(
      '#type' => 'textfield',
      '#description' => t('Also known as "Breakpoint".') . ' <em>(' . t('Default') . ': 768)</em>',
      '#default_value' => $config['smallbp'],
      '#field_suffix' => t('pixels'),
      '#size' => 10,
    );
    $form['sf-plugins']['sf-smallscreen']['sf-smallscreen-useragent'] = array(
      '#type' => 'details',
      '#title' => t('User agent settings'),
    );
    $form['sf-plugins']['sf-smallscreen']['sf-smallscreen-useragent']['smallua'] = array(
      '#type' => 'radios',
      '#default_value' => $config['smallua'],
      '#options' => array(
        0 => t('Use the pre-defined list of the <strong>user agents</strong>.') . '<sup>(' . t('Default') . ') (' . t('Recommended') . ')</sup>',
        1 => t('Use the custom list of the <strong>user agents</strong>.'),
      ),
    );
    $form['sf-plugins']['sf-smallscreen']['sf-smallscreen-useragent']['smallual'] = array(
      '#type' => 'textfield',
      '#title' => t('Custom list of the user agents'),
      '#description' => t('Could be partial or complete. (Asterisk separated)') . ' <em>(' . t('Default') . ': ' . t('empty') . ')</em><br />' . t('Examples') . ':<ul><li>iPhone*Android*iPad <sup>(' . t('Recommended') . ')</sup></li><li>Mozilla/5.0 (webOS/1.4.0; U; en-US) AppleWebKit/532.2 (KHTML, like Gecko) Version/1.0 Safari/532.2 Pre/1.0 * Mozilla/5.0 (iPad; U; CPU OS 3_2_1 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Mobile/7B405</li></ul>' . ((isset($_SERVER['HTTP_USER_AGENT'])) ? '<br />' . t('<strong>UA string of the current Web browser:</strong>') . ' ' . $_SERVER['HTTP_USER_AGENT'] : ''),
      '#default_value' => $config['smallual'],
      '#size' => 100,
      '#maxlength' => 2000,
    );
    $form['sf-plugins']['sf-smallscreen']['sf-smallscreen-useragent']['smalluam'] = array(
      '#type' => 'select',
      '#title' => t('<strong>User agent</strong> detection method'),
      '#description' => '<em>(' . t('Default') . ': ' . t('Client-side (JavaScript)') . ')</em>',
      '#default_value' => $config['smalluam'],
      '#options' => array(
        0 => t('Client-side (JavaScript)'),
        1 => t('Server-side (PHP)')
      ),
    );
    $form['sf-plugins']['sf-smallscreen']['smallact'] = array(
      '#type' => 'radios',
      '#title' => t('Select a type'),
      '#default_value' => $config['smallact'],
      '#options' => array(
        0 => t('Convert the menu to a &lt;select&gt; element.'),
        1 => t('Convert the menu to an accordion menu.') . ' <sup>(' . t('Default') . ')</sup>',
      ),
    );
    $form['sf-plugins']['sf-smallscreen']['sf-smallscreen-select'] = array(
      '#type' => 'details',
      '#title' => t('&lt;select&gt; settings'),
    );
    $form['sf-plugins']['sf-smallscreen']['sf-smallscreen-select']['smallset'] = array(
      '#type' => 'textfield',
      '#title' => t('&lt;select&gt; title'),
      '#description' => t('By default the first item in the &lt;select&gt; element will be the name of the parent menu or the title of this block, you can change this by setting a custom title.') . ' <em>(' . t('Default') . ': ' . t('empty') . ')</em><br />' . t('Example') . ': <em> - ' . t('Main Menu') . ' - </em>.',
      '#default_value' => $config['smallset'],
      '#size' => 50,
      '#maxlength' => 500,
    );
    $form['sf-plugins']['sf-smallscreen']['sf-smallscreen-select']['smallasa'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add <em>selected</em> attribute to the &lt;option&gt; element with the class <strong>active</strong> .'),
      '#description' => t('Makes pre-selected the item linked to the active page when the page loads.'),
      '#default_value' => $config['smallasa'],
    );
    $form['sf-plugins']['sf-smallscreen']['sf-smallscreen-select']['sf-smallscreen-select-more'] = array(
      '#type' => 'details',
      '#title' => t('More'),
      '#open' => TRUE,
    );
    $form['sf-plugins']['sf-smallscreen']['sf-smallscreen-select']['sf-smallscreen-select-more']['smallcmc'] = array(
      '#type' => 'checkbox',
      '#title' => t('Copy the main &lt;ul&gt; classes to the &lt;select&gt;.') . ' <sup><em>(' . t('Default') . ': ' . t('disabled') . ')</em></sup>',
      '#default_value' => $config['smallcmc'],
    );
    $form['sf-plugins']['sf-smallscreen']['sf-smallscreen-select']['sf-smallscreen-select-more']['smallecm'] = array(
      '#type' => 'textfield',
      '#title' => t('Exclude these classes from the &lt;select&gt; element'),
      '#description' => t('Comma separated') . ' <em>(' . t('Default') . ': ' . t('empty') . ')</em>',
      '#default_value' => $config['smallecm'],
      '#size' => 100,
      '#maxlength' => 1000,
      '#states' => array(
        'enabled' => array(
          ':input[name="superfish_smallcmc' . '"]' => array('checked' => TRUE),
        ),
      ),
    );
    $form['sf-plugins']['sf-smallscreen']['sf-smallscreen-select']['sf-smallscreen-select-more']['smallchc'] = array(
      '#type' => 'checkbox',
      '#title' => t('Copy the hyperlink classes to the &lt;option&gt; elements of the &lt;select&gt;.') . ' <sup><em>(' . t('Default') . ': ' . t('disabled') . ')</em></sup>',
      '#default_value' => $config['smallchc'],
    );
    $form['sf-plugins']['sf-smallscreen']['sf-smallscreen-select']['sf-smallscreen-select-more']['smallech'] = array(
      '#type' => 'textfield',
      '#title' => t('Exclude these classes from the &lt;option&gt; elements of the &lt;select&gt;'),
      '#description' => t('Comma separated') . ' <em>(' . t('Default') . ': ' . t('empty') . ')</em>',
      '#default_value' => $config['smallech'],
      '#size' => 100,
      '#maxlength' => 1000,
      '#states' => array(
        'enabled' => array(
          ':input[name="superfish_smallchc' . '"]' => array('checked' => TRUE),
        ),
      ),
    );
    $form['sf-plugins']['sf-smallscreen']['sf-smallscreen-select']['sf-smallscreen-select-more']['smallicm'] = array(
      '#type' => 'textfield',
      '#title' => t('Include these classes in the &lt;select&gt; element'),
      '#description' => t('Comma separated') . ' <em>(' . t('Default') . ': ' . t('empty') . ')</em>',
      '#default_value' => $config['smallicm'],
      '#size' => 100,
      '#maxlength' => 1000,
    );
    $form['sf-plugins']['sf-smallscreen']['sf-smallscreen-select']['sf-smallscreen-select-more']['smallich'] = array(
      '#type' => 'textfield',
      '#title' => t('Include these classes in the &lt;option&gt; elements of the &lt;select&gt;'),
      '#description' => t('Comma separated') . ' <em>(' . t('Default') . ': ' . t('empty') . ')</em>',
      '#default_value' => $config['smallich'],
      '#size' => 100,
      '#maxlength' => 1000,
    );
    $form['sf-plugins']['sf-smallscreen']['sf-smallscreen-accordion'] = array(
      '#type' => 'details',
      '#title' => t('Accordion settings'),
    );
    $form['sf-plugins']['sf-smallscreen']['sf-smallscreen-accordion']['smallamt'] = array(
      '#type' => 'textfield',
      '#title' => t('Accordion menu title'),
      '#description' => t('By default the caption of the accordion toggle switch will be the name of the parent menu or the title of this block, you can change this by setting a custom title.') . ' <em>(' . t('Default') . ': ' . t('empty') . ')</em><br />' . t('Example') . ': <em>' . t('Menu') . '</em>.',
      '#default_value' => $config['smallamt'],
      '#size' => 50,
      '#maxlength' => 500,
    );
    $form['sf-plugins']['sf-smallscreen']['sf-smallscreen-accordion']['smallabt'] = array(
      '#type' => 'radios',
      '#title' => t('Accordion button type'),
      '#default_value' => $config['smallabt'],
      '#options' => array(
        0 => t('Use parent menu items as buttons.'),
        1 => t('Use parent menu items as buttons, add cloned parent links to sub-menus as well.') . ' <sup>(' . t('Default') . ')</sup>',
        2 => t('Create new links next to parent menu item links and use them as buttons.'),
      ),
    );
    $form['sf-plugins']['sf-supersubs'] = array(
      '#type' => 'details',
      '#title' => t('Supersubs'),
      '#description' => t('<strong>Supersubs</strong> makes it possible to define custom widths for your menus.'),
      '#open' => TRUE,
    );
    $form['sf-plugins']['sf-supersubs']['supersubs'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Supersubs for this menu.'),
      '#default_value' => $config['supersubs'],
    );
    $form['sf-plugins']['sf-supersubs']['minwidth'] = array(
      '#type' => 'textfield',
      '#title' => t('Minimum width'),
      '#description' => t('Minimum width for sub-menus, in <strong>em</strong> units.') . ' <em>(' . t('Default') . ': 12)</em>',
      '#default_value' => $config['minwidth'],
      '#size' => 10,
    );
    $form['sf-plugins']['sf-supersubs']['maxwidth'] = array(
      '#type' => 'textfield',
      '#title' => t('Maximum width'),
      '#description' => t('Maximum width for sub-menus, in <strong>em</strong> units.') . ' <em>(' . t('Default') . ': 27)</em>',
      '#default_value' => $config['maxwidth'],
      '#size' => 10,
    );
    $form['sf-megamenu'] = array(
      '#type' => 'details',
      '#title' => t('Multi-column sub-menus') . ' <sup>(' . t('Beta') . ')</sup>',
    );
    $form['sf-megamenu']['multicolumn'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable multi-column sub-menus.'),
      '#default_value' => $config['multicolumn'],
    );
    $form['sf-megamenu']['mcexclude'] = array(
      '#type' => 'textfield',
      '#title' => t('Exclude below menu items'),
      '#description' => t('Enter the ID number of the parent menu item you <strong>do not</strong> want multi-column sub-menus for. (Comma separated)') . ' <em>(' . t('Default') . ': ' . t('empty') . ')</em><br />' . t('Example') . ': 5,10,20',
      '#default_value' => $config['mcexclude'],
      '#size' => 50,
    );
    $form['sf-megamenu']['mcdepth'] = array(
      '#type' => 'select',
      '#title' => t('Start from depth'),
      '#description' => t('The depth of the first instance of multi-column sub-menus.') . ' <em>(' . t('Default') . ': 1)</em>',
      '#default_value' => $config['mcdepth'],
      '#options' => drupal_map_assoc(range(1, 10)),
    );
    $form['sf-megamenu']['mclevels'] = array(
      '#type' => 'select',
      '#title' => t('Levels'),
      '#description' => t('The amount of sub-menu levels that will be included in the multi-column sub-menu.') . ' <em>(' . t('Default') . ': 1)</em>',
      '#default_value' => $config['mclevels'],
      '#options' => drupal_map_assoc(range(1, 10)),
    );
    $form['sf-advanced-html'] = array(
      '#type' => 'details',
      '#title' => t('Advanced HTML settings'),
    );
    $form['sf-advanced-html']['sf-hyperlinks'] = array(
      '#type' => 'details',
      '#title' => t('Hyperlinks'),
      '#open' => TRUE,
    );
    $form['sf-advanced-html']['sf-hyperlinks']['hhldescription'] = array(
      '#type' => 'checkbox',
      '#title' => t('Hide hyperlink descriptions ("title" attribute)') . ' <em>(' . t('Default') . ': ' . t('disabled') . ')</em>',
      '#description' => t('(Enabling this feature makes the below settings ineffective.)'),
      '#default_value' => $config['hhldescription'],
    );
    $form['sf-advanced-html']['sf-hyperlinks']['sf-hyperlinktexts'] = array(
      '#type' => 'details',
      '#title' => t('Hyperlink texts'),
      '#open' => TRUE,
    );
    $form['sf-advanced-html']['sf-hyperlinks']['sf-hyperlinktexts']['hldescription'] = array(
      '#type' => 'checkbox',
      '#title' => t('Insert hyperlink descriptions ("title" attribute) into hyperlink texts.'),
      '#default_value' => $config['hldescription'],
    );
    $form['sf-advanced-html']['sf-hyperlinks']['sf-hyperlinktexts']['hldmenus'] = array(
      '#type' => 'textfield',
      '#title' => t('Limit to below menu items'),
      '#description' => t('Enter menu item ID\'s. Leave empty to include all menus. (Comma separated)') . ' <em>(' . t('Default') . ': ' . t('empty') . ')</em><br />' . t('Example') . ': 5,10,20',
      '#default_value' => $config['hldmenus'],
      '#size' => 50,
    );
    $form['sf-advanced-html']['sf-hyperlinks']['sf-hyperlinktexts']['hldexclude'] = array(
      '#type' => 'textfield',
      '#title' => t('Exclude below menu items'),
      '#description' => t('Enter the ID of the menu items you <strong>do not</strong> want to link descriptions for. (Comma separated)') . ' <em>(' . t('Default') . ': ' . t('empty') . ')</em><br />' . t('Example') . ': 5,10,20',
      '#default_value' => $config['hldexclude'],
      '#size' => 50,
    );
    $form['sf-advanced-html']['sf-html-wrappers'] = array(
      '#type' => 'details',
      '#title' => t('HTML wrappers'),
      '#open' => TRUE,
    );
    $form['sf-advanced-html']['sf-html-wrappers']['wrapmul'] = array(
      '#type' => 'textfield',
      '#title' => t('Around the main &lt;UL&gt;'),
      '#description' => t('Insert extra codes <strong>before</strong> and\or <strong>after</strong> the main UL. (Comma separated).') . ' <em>(' . t('Default') . ': ' . t('empty') . ')</em><br />' . t('Examples') . ': <ul><li>&lt;h3&gt;Discover the universe!&lt;/h3&gt;,</li><li>&lt;h3&gt;Hello there!&lt;/h3&gt;,&lt;div style="clear:both"&gt;&lt;/div&gt;</li><li>,&lt;div style="clear:both"&gt;&lt;/div&gt;</li></ul>',
      '#default_value' => $config['wrapmul'],
      '#size' => 100,
      '#maxlength' => 1000,
    );
    $form['sf-advanced-html']['sf-html-wrappers']['wrapul'] = array(
      '#type' => 'textfield',
      '#title' => t('Around the &lt;UL&gt; content'),
      '#description' => t('Insert extra codes <strong>before</strong> and\or <strong>after</strong> the ULs. (Comma separated).') . ' <em>(' . t('Default') . ': ' . t('empty') . ')</em><br />' . t('Examples') . ': <ul><li>&lt;h3&gt;Discover the universe!&lt;/h3&gt;,</li><li>&lt;h3&gt;Hello there!&lt;/h3&gt;,&lt;div style="clear:both"&gt;&lt;/div&gt;</li><li>,&lt;div style="clear:both"&gt;&lt;/div&gt;</li></ul>',
      '#default_value' => $config['wrapul'],
      '#size' => 100,
      '#maxlength' => 1000,
    );
    $form['sf-advanced-html']['sf-html-wrappers']['wraphl'] = array(
      '#type' => 'textfield',
      '#title' => t('Around the hyperlinks'),
      '#description' => t('Insert HTML objects <strong>before</strong> and\or <strong>after</strong> hyperlinks. (Comma separated)') . ' <em>(' . t('Default') . ': ' . t('empty') . ')</em><br />' . t('Examples') . ': <ul><li>&lt;span class="background-left"&gt;&lt;span class="background-right"&gt;,&lt;/span&gt;&lt;/span&gt;</li><li>&lt;img src="example.jpg" width="24" height="24" alt="example" title="example" /&gt;,</li><li>,&lt;span class="custom-arrow"&gt;>&lt;/span&gt;</li></ul>',
      '#default_value' => $config['wraphl'],
      '#size' => 100,
      '#maxlength' => 1000,
    );
    $form['sf-advanced-html']['sf-html-wrappers']['wraphlt'] = array(
      '#type' => 'textfield',
      '#title' => t('Around the hyperlinks content'),
      '#description' => t('Insert extra codes <strong>before</strong> and\or <strong>after</strong> the text in hyperlinks. (Comma separated)') . ' <em>(' . t('Default') . ': ' . t('empty') . ')</em><br />' . t('Examples') . ': <ul><li>&lt;span class="background-left"&gt;&lt;span class="background-right"&gt;,&lt;/span&gt;&lt;/span&gt;</li><li>&lt;img src="example.jpg" width="24" height="24" alt="example" title="example" /&gt;,</li><li>,&lt;span class="custom-arrow"&gt;>&lt;/span&gt;</li></ul>',
      '#default_value' => $config['wraphlt'],
      '#size' => 100,
      '#maxlength' => 1000,
    );
    $form['sf-advanced-css'] = array(
      '#type' => 'details',
      '#title' => t('Advanced CSS settings'),
    );
    $form['sf-advanced-css']['sf-helper-classes'] = array(
      '#type' => 'details',
      '#title' => t('Helper classes'),
      '#open' => TRUE,
    );
    $form['sf-advanced-css']['sf-helper-classes']['firstlast'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add <strong>first \ middle \ last</strong> classes.'),
      '#default_value' => $config['firstlast'],
    );
    $form['sf-advanced-css']['sf-helper-classes']['zebra'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add <strong>odd \ even</strong> classes.'),
      '#default_value' => $config['zebra'],
    );
    $form['sf-advanced-css']['sf-helper-classes']['dfirstlast'] = array(
      '#type' => 'checkbox',
      '#title' => t('Do <strong>not</strong> add <strong>first \ middle \ last</strong> classes to single menu items.'),
      '#default_value' => $config['dfirstlast'],
    );
    $form['sf-advanced-css']['sf-helper-classes']['dzebra'] = array(
      '#type' => 'checkbox',
      '#title' => t('Do <strong>not</strong> add <strong>odd \ even</strong> classes to single menu items.'),
      '#default_value' => $config['dzebra'],
    );
    $form['sf-advanced-css']['sf-helper-classes']['itemcount'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add <strong>item count</strong> class to menu items.') . '<em>(sf-item-1, sf-item-2, sf-item-3, ...)</em>',
      '#default_value' => $config['itemcount'],
    );
    $form['sf-advanced-css']['sf-helper-classes']['itemcounter'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add <strong>children counter</strong> classes to menu items.') . '<em>(sf-total-children-7 sf-parent-children-4 sf-single-children-3, ...)</em>',
      '#default_value' => $config['itemcounter'],
    );
    $form['sf-advanced-css']['sf-helper-classes']['itemdepth'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add <strong>item depth</strong> class to menu items and their hyperlinks.') . '<em>(sf-depth-1, sf-depth-2, sf-depth-3, ...)</em>',
      '#default_value' => $config['itemdepth'],
    );
    $form['sf-advanced-css']['sf-custom-classes'] = array(
      '#type' => 'details',
      '#title' => t('Custom classes'),
      '#open' => TRUE,
    );
    $form['sf-advanced-css']['sf-custom-classes']['ulclass'] = array(
      '#type' => 'textfield',
      '#title' => t('For the main UL'),
      '#description' => t('(Space separated, without dots)') . ' <em>(' . t('Default') . ': ' . t('empty') . ')</em><br />' . t('Example') . ': top-menu category-science',
      '#default_value' => $config['ulclass'],
      '#size' => 50,
      '#maxlength' => 1000,
    );
    $form['sf-advanced-css']['sf-custom-classes']['liclass'] = array(
      '#type' => 'textfield',
      '#title' => t('For the list items'),
      '#description' => t('(Space separated, without dots)') . ' <em>(' . t('Default') . ': ' . t('empty') . ')</em><br />' . t('Example') . ': science-sub',
      '#default_value' => $config['liclass'],
      '#size' => 50,
      '#maxlength' => 1000,
    );
    $form['sf-advanced-css']['sf-custom-classes']['hlclass'] = array(
      '#type' => 'textfield',
      '#title' => t('For the hyperlinks'),
      '#description' => t('(Space separated, without dots)') . ' <em>(' . t('Default') . ': ' . t('empty') . ')</em><br />' . t('Example') . ': science-link',
      '#default_value' => $config['hlclass'],
      '#size' => 50,
      '#maxlength' => 1000,
    );
    $form['sf-advanced-css']['sf-extra-css'] = array(
      '#type' => 'details',
      '#title' => t('Extra CSS'),
    );
    $form['sf-advanced-css']['sf-extra-css']['pathcss'] = array(
      '#type' => 'textfield',
      '#title' => t('Path to CSS file(s)'),
      '#description' => t('Include extra CSS file(s). (Comma separated)') . ' <em>(' . t('Default') . ': ' . t('empty') . ')</em><br />' . t('Examples') . ': <ul><li>sites/all/files/example.css</li><li>sites/all/files/example.css,sites/all/files/example2.css</li></ul>',
      '#default_value' => $config['pathcss'],
      '#size' => 100,
      '#maxlength' => 1000,
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, &$form_state) {
    $supersubs_error = FALSE;
    $values = $form_state['values'];dsm($values);

    $style = $values['sf-settings']['style'];
    $speed = $values['sf-settings']['speed'];
    $delay = $values['sf-settings']['delay'];
    $pathclass = $values['sf-settings']['pathclass'];
    $touch = $values['sf-plugins']['sf-touchscreen']['touch'];
    $touchbp = $values['sf-plugins']['sf-touchscreen']['sf-touchscreen-windowwidth']['touchbp'];
    $touchua = $values['sf-plugins']['sf-touchscreen']['sf-touchscreen-useragent']['touchua'];
    $touchual = $values['sf-plugins']['sf-touchscreen']['sf-touchscreen-useragent']['touchual'];
    $small = $values['sf-plugins']['sf-smallscreen']['small'];
    $smallbp = $values['sf-plugins']['sf-smallscreen']['sf-smallscreen-windowwidth']['smallbp'];
    $smallua = $values['sf-plugins']['sf-smallscreen']['sf-smallscreen-useragent']['smallua'];
    $smallual = $values['sf-plugins']['sf-smallscreen']['sf-smallscreen-useragent']['smallual'];
    $minwidth = $values['sf-plugins']['sf-supersubs']['minwidth'];
    $maxwidth = $values['sf-plugins']['sf-supersubs']['maxwidth'];
    $mcexclude = $values['sf-megamenu']['mcexclude'];
    $hldmenus = $values['sf-advanced-html']['sf-hyperlinks']['sf-hyperlinktexts']['hldmenus'];
    $hldexclude = $values['sf-advanced-html']['sf-hyperlinks']['sf-hyperlinktexts']['hldexclude'];
    $extracss = $values['sf-advanced-css']['sf-extra-css']['pathcss'];

    if ($style != 'none' && superfish_styles('path', $style) == '') {
      form_set_error('settings][sf-settings][style', t('Cannot find the CSS file of the selected style.'));
    }
    elseif (!is_numeric($speed) && !in_array($speed, array('slow', 'normal', 'fast'))) {
      form_set_error('settings][sf-settings][speed', t('<strong>Animation speed</strong>: unacceptable value entered.'));
    }
    if (!is_numeric($delay)) {
      form_set_error('settings][sf-settings][delay', t('<strong>Mouse delay</strong>: unacceptable value entered.'));
    }
    if (is_numeric($pathclass)) {
      form_set_error('settings][sf-settings][pathclass', t('<strong>Path class</strong>: unacceptable value entered.'));
    }
    if ($touch == 2 && $touchbp == '') {
      form_set_error('settings][sf-plugins][sf-touchscreen][sf-touchscreen-useragent][touchbp', t('<strong>sf-Touchscreen Breakpoint</strong>: field cannot be empty.'));
    }
    if (!is_numeric($touchbp)) {
      form_set_error('settings][sf-plugins][sf-touchscreen][sf-touchscreen-useragent][touchbp', t('<strong>sf-Touchscreen Breakpoint</strong>: unacceptable value entered.'));
    }
    if ($touch == 3 && $touchua == 1 && $touchual == '') {
      form_set_error('settings][sf-plugins][sf-touchscreen][sf-touchscreen-useragent][touchual', t('<strong>sf-Touchscreen Custom list of user agents</strong>: field cannot be empty.'));
    }
    if ($small == 2 && $smallbp == '') {
      form_set_error('settings][sf-plugins][sf-smallscreen][sf-smallscreen-windowwidth][smallbp', t('<strong>sf-Smallscreen Breakpoint</strong>: field cannot be empty.'));
    }
    if (!is_numeric($smallbp)) {
      form_set_error('settings][sf-plugins][sf-smallscreen][sf-smallscreen-windowwidth][smallbp', t('<strong>sf-Smallscreen Breakpoint</strong>: unacceptable value entered.'));
    }
    if ($small == 3 && $smallua == 1 && $smallual == '') {
      form_set_error('settings][sf-plugins][sf-smallscreen][sf-smallscreen-useragent][smallual', t('<strong>sf-Smallscreen Custom list of user agents</strong>: field cannot be empty.'));
    }
    if (!is_numeric($minwidth)) {
      form_set_error('minwidth', t('<strong>Supersubs minimum width</strong>: unacceptable value entered.'));
      $supersubs_error = TRUE;
    }
    if (!is_numeric($maxwidth)) {
      form_set_error('maxwidth', t('<strong>Supersubs maximum width</strong>: unacceptable value entered.'));
      $supersubs_error = TRUE;
    }
    if ($supersubs_error !== TRUE && $minwidth > $maxwidth) {
      form_set_error('maxwidth', t('Supersubs <strong>maximum width</strong> has to be bigger than the <strong>minimum width</strong>.'));
    }
    if (!empty($mcexclude) && (!is_numeric(str_replace(',', '', $mcexclude)) || strpos($mcexclude, ' .'))) {
      form_set_error('mcexclude', t('<strong>Multi-column subs-menus</strong>: unacceptable value entered.'));
    }
    if (!empty($hldmenus) && (!is_numeric(str_replace(',', '', $hldmenus)) || strpos($hldmenus, ' .'))) {
      form_set_error('hldmenus', t('<strong>Hyperlinks</strong>: unacceptable value entered.'));
    }
    if (!empty($hldexclude) && (!is_numeric(str_replace(',', '', $hldexclude)) || strpos($hldexclude, ' .'))) {
      form_set_error('hldexclude', t('<strong>Hyperlinks</strong>: unacceptable value entered.'));
    }
    if (!empty($extracss)) {
      if (strpos($extracss, ',')) {
        $error = array();
        $extracss = array_remove_empty(explode(',', $extracss));
        foreach ($extracss as $c) {
          if (!file_exists($c)) {
            $error[] = $c;
          }
        }
        if (!empty($error)) {
          form_set_error('pathcss', t('Cannot find the CSS file mentioned in <strong>Extra CSS</strong>'));
        }
      }
      elseif (!file_exists($extracss)) {
        form_set_error('pathcss', t('Cannot find the CSS file mentioned in <strong>Extra CSS</strong>'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, &$form_state) {
    parent::blockSubmit($form, $form_state);

    $this->saveValuesRecursively($form_state['values']);
  }

  public function defaultConfiguration() {
    $config = array();
    $config['name'] = 'Superfish menu';
    $config['menu'] = array('mlid' => 0);
    $config['depth'] = -1;
    $config['expanded'] = 0;
    $config['type'] = 'horizontal';
    $config['style'] = 'none';
    $config['speed'] = 'fast';
    $config['delay'] = 800;
    $config['pathclass'] = 'active-trail';
    $config['pathlevels'] = 1;
    $config['slide'] = 'vertical';
    $config['arrow'] = 1;
    $config['shadow'] = 1;
    $config['use_link_theme'] = 1;
    $config['use_item_theme'] = 1;
    $config['clone_parent'] = 0;
    $config['bgf'] = 0;
    $config['spp'] = 1;
    $config['hid'] = 1;
    $config['amw'] = 0;
    $config['touch'] = 0;
    $config['touchbh'] = 2;
    $config['touchbp'] = 768;
    $config['touchua'] = 0;
    $config['touchual'] = '';
    $config['touchuam'] = 0;
    $config['small'] = 2;
    $config['smallbp'] = 768;
    $config['smallua'] = 0;
    $config['smallual'] = '';
    $config['smalluam'] = 0;
    $config['smallact'] = 1;
    $config['smallset'] = '';
    $config['smallasa'] = 0;
    $config['smallcmc'] = 0;
    $config['smallecm'] = '';
    $config['smallchc'] = 0;
    $config['smallech'] = '';
    $config['smallicm'] = '';
    $config['smallich'] = '';
    $config['smallamt'] = '';
    $config['smallabt'] = 1;
    $config['supersubs'] = 1;
    $config['minwidth'] = 12;
    $config['maxwidth'] = 27;
    $config['multicolumn'] = 0;
    $config['mcexclude'] = '';
    $config['mcdepth'] = 1;
    $config['mclevels'] = 1;
    $config['hhldescription'] = 0;
    $config['hldescription'] = 0;
    $config['hldmenus'] = '';
    $config['hldexclude'] = '';
    $config['wrapmul'] = '';
    $config['wrapul'] = '';
    $config['wraphl'] = '';
    $config['wraphlt'] = '';
    $config['firstlast'] = 1;
    $config['zebra'] = 1;
    $config['dfirstlast'] = 0;
    $config['dzebra'] = 0;
    $config['itemcount'] = 1;
    $config['itemcounter'] = 1;
    $config['itemdepth'] = 1;
    $config['ulclass'] = '';
    $config['liclass'] = '';
    $config['hlclass'] = '';
    $config['pathcss'] = '';

    return $config;
  }

  private function saveValuesRecursively($values) {
    $config_names = array_keys($this->defaultConfiguration());
    foreach($values as $key => $value) {
      if (is_array($value)) {
        $this->saveValuesRecursively($value);
      }
      else {
        if (in_array($key, $config_names)) {
          $this->setConfigurationValue($key, $value);
        }
      }
    }
  }
}